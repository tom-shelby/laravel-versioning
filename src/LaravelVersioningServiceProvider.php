<?php

namespace TomShelby\LaravelVersioning;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use TomShelby\LaravelVersioning\Commands\VersionGetCommand;
use TomShelby\LaravelVersioning\Services\Version;
use TomShelby\LaravelVersioning\Facades\VersionFacade;

class LaravelVersioningServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // if($this->app->runningInConsole()) {
            $this->commands([
                VersionGetCommand::class,
            ]);
        // }

        // Config
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('versioning.php'),
        ]);
        // Translations
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'laravel-versioning');
        $this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/laravel-versioning'),
        ]);
    }

    public function register()
    {
        $this->app->singleton(VersionFacade::class, function(Application $app) {
            return new Version();
        });

        $this->app->bind('version', function(Application $app) {
            return new Version();
        });

        Blade::directive('version', 
            function () {
                $version = Version::get();
                if(is_null($version)) {
                    $path = config_path('versioning');
                    throw new Exception("Please set default values inside : ${path}");
                }
                $message = "<?php print '${version}' ?>";
                return $message;
            }
        );
    }

}
