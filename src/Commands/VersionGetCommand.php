<?php

namespace TomShelby\LaravelVersioning\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use TomShelby\LaravelVersionning\Services\ProjectVersionService;

class VersionGetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'version:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Return the current version (current git tag) as a string';

    private $process;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->afterInitialization();
    }

    public function afterInitialization()
    {
        $this->process = new Process(["git", "describe", "--tags","--abbrev=0"]);
        $this->process->setWorkingDirectory(base_path());
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->process->run();
        if(!$this->process->isSuccessful()) {
            throw new ProcessFailedException($this->process);
        }
        $gitTag = trim($this->process->getOutput());
        if(!Cache::has('application-version') || Cache::get('application-version') !== $gitTag) {
            Cache::forever('application-version', $gitTag);
        }

        $this->output->writeln(trans('laravel-versioning::general.informations.currentVersion') . " : <fg=#c0329b>$gitTag</>");
    }
}
