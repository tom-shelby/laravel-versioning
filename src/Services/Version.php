<?php

namespace TomShelby\LaravelVersioning\Services;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class Version {

    public static function get()
    {
        Artisan::call('view:clear');
        return Cache::get('application-version') ?? Config::get('versioning.no_version_value');
    }

}