## Install via composer :

`composer require tom-shelby/laravel-versionning`

---

## Configuration

`php artisan vendor:publish`

Published configuration file :
`config/versionning.php`

Published language files (FR/EN) :
`resources/lang/vendor/laravel-versionning/`

---

## Commands

- Retrieve and cache version from git for services :
 `php artisan vendor:get`

## Services

- To use inside Blade files : `@version`

<!-- - To use inside your application's logic (Controllers...) : `use TomShelby\LaravelVersionning\Services\ProjectVersionService;` -->

